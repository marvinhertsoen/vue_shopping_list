export default {
    add (list) {
        let lists = JSON.parse(localStorage.getItem('lists'));
        if(!lists){
            lists = [];
        }

        list.id = this.getLastId();
        list.elements = [];

        lists.push(list);
        localStorage.setItem('lists', JSON.stringify(lists));
    },
    get(id){
        let lists = this.getAll();
        return lists.find(o => o.id == id);
    },
    getAll(){
        return this.readLists();
    },
    remove(id){
        let lists = this.getAll();
        let listIndex = lists.findIndex(o => o.id == id);
        lists.splice(listIndex, 1);
        localStorage.setItem('lists', JSON.stringify(lists));
    },
    update(list){
        let lists = this.getAll();
        let index = lists.findIndex(o => o.id == list.id);
        lists[index] = list;
        localStorage.setItem('lists', JSON.stringify(lists));
    },

    /**
     * Read lists from LocalStorage
     *
     * @returns {any}
     */
    readLists(){
        return JSON.parse(localStorage.getItem('lists'));
    },

    getLastId(){
        let lists = JSON.parse(localStorage.getItem('lists'));
        if(lists){
            return Math.max.apply(Math, lists.map(function(o) { return o.id; })) + 1
        }
        return 0;
    }
}
