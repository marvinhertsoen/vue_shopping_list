import ListService from '../services/ListService'

export default {
    add (element, listId) {
        let list = ListService.get(listId);
        element.id = this.getNextId(listId);
        list.elements.push(element);
        ListService.update(list)
    },
    remove(id, listId){
        let list = ListService.get(listId);
        let elementIndex = list.elements.findIndex(o => o.id == id);
        list.elements.splice(elementIndex, 1);
        ListService.update(list)
    },
    getNextId(listId){
        let list = ListService.get(listId);
        if(list.elements.length > 0){
            let lastId = Math.max.apply(Math, list.elements.map(function(o) { return o.id; }));
            return lastId + 1;
        }
        return 0;
    }
}
