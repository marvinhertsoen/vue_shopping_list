import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

export default new Router({
    mode: 'history',

    routes: [
        {
            path: '/',
            name: 'Accueil',
            component: require('../pages/HomePage.vue').default,
            meta: {
                title: 'Accueil',
            }
        },
        {
            path: '/listing',
            name: 'lising',
            component: require('../pages/Pagelist.vue').default,
            meta: {
                title: 'Toutes les listes'
            }
        },
        {
            path: '/new',
            name: 'New List',
            component: require('../pages/Newlist.vue').default,
            meta: {
                title: 'Nouvelle Liste'
            }
        },
        {
            path: '/list/:slug',
            props: true,
            name: 'View List',
            component: require('../pages/Viewlist.vue').default,
            meta: {
                title: 'Liste'
            }
        },
        {
            path: '/list/:slug/add',
            props:true,
            name: 'Add list Element',
            component: require('../pages/NewListElement.vue').default,
            meta: {
                title: 'Ajout d\'un élément'
            }
        },
        {
            path: '*',
            redirect: '/'
        }
    ]
})
