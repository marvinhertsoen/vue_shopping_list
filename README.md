# My Shopping List

Cette application est mon implémentation d'une shopping list en VueJS.

Voici les différentes fonctionnalités :

**Création de listes**

La page de création de liste permet d'ajouter une liste de course en renseignant certaines informations comme:

    Son nom
    Sa description (facultatif)
    Une image d'illustration (facultatif)

**Manipulations des listes**

La page de listing permet d'afficher les informations des différentes listes de courses, ainsi que de créer ou supprimer ces dernières.

Outre les informations de base de chaque liste, on y retrouve également:

    Le nombre d'articles contenus
    Le prix total des articles

**Les éléments**

Chaque liste de course peut être constituée de différents éléments.

Un élément peut être ajouté ou supprimé dans l'écran de la liste à laquelle il appartient.

Il est défini par :

    Un nom
    Une description (facultatif)
    Un prix

Information : Un élément ne peut être sauvegardé que si son nom et son prix sont renseignés.

**Informations techniques**

Le layout (header-navbar/footer) est statique. Seul le contenu change entre 2 pages (vue-router).

Le Localstorage a été utilisé pour stocker les listes et leurs éléments.

J'ai créé une couche service pour abstraire l'accès / modification des données depuis le Localstorage (cf. dossier /services/* )

**Travail supplémentaire**

Gestion des images de listes + définition d'une image par défaut si le champ n'est pas / est mal renseigné à la création.

Ajout de la notion de prix d'articles + affichage du total dans la page de listing pour chacune des listes

Notion de suppression de liste 

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
